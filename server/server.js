var loopback = require('loopback');
var boot = require('loopback-boot');
var bodyParser = require('body-parser');
var handlers = require(__dirname + '/handlers.js');

var app = module.exports = loopback();

app.use(bodyParser.json());

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    console.log('ciao');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};


app.post('/', handlers.a);

app.get('/targa/:targa*?', handlers.b);

app.post('/licenseUpdate', handlers.c);

app.post('/saveGarage', handlers.d);

app.get('/codice/:codice*?', handlers.e);

app.post('/insertDossiers', handlers.f);

app.post('/saveDossiers', handlers.g);


// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
