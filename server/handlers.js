var excelbuilder = require('msexcel-builder');
var fs = require('fs');
var db = require(__dirname + '/db.js');

var handlers = module.exports = {


    a: function(req, res) {
	
        var err = new Error();
        fs.writeFile('log.txt', JSON.stringify(req.body));
        
		//Check sulla request
        if (!req.body) {
            err.message = "Richiesta non valida";
            err.status = 400;
            console.log(err.message);
            res.send(err);
            return res.end();

        }

        if (!Array.isArray(req.body)) {
            err.message = "Non � un array";
            err.status = 400;
            console.log(err.message);
            res.send(err);
            return res.end();
        }

        if (req.body.length <= 0) {
            err.message = "L'array � vuoto";
            err.status = 400;
            console.log(err.message);
            res.send(err);
            return res.end();
        }
        //creazione a priori di un workbook che conterr� il nostro file Excel o pi� file di questo tipo  (sheet1).
        var workbook = excelbuilder.createWorkbook('./File_Excel', 'sample.xlsx');
        var col = Object.keys(req.body[0]).length;
        console.log(col);
        var sheet1 = workbook.createSheet('sheet1', col, req.body.length);


        //console.log(sheet1);
        //console.log(req.body);

        //var length = Object.keys(json).length;
        //sheet1.set(1, 1, 'Informazioni');

        for (var i = 0; i < req.body.length; i++) {

            var k = i + 1;
            var obj = req.body[i];
            var j = 1;

            for (var prop in obj) {
                //console.log(obj[prop]);
                //var nome=JSON.stringify(obj[prop].name);
                //console.log(nome);
                sheet1.set(j, k, JSON.stringify(obj[prop]));
                j++;

            }
        }
        console.log('wooooooooooooooooork');

		//Salvataggio dei dati dello sheet (operazione necessaria)
        workbook.save(function(ok) {
            if (!ok)
                workbook.cancel();
            else
                console.log('congratulations, your workbook created');
        });

        //console.log(JSON.stringify(sheet1));
    },

    b: function getResourcesAction(req, res) {

        var err = new Error();
        fs.writeFile('log.txt', JSON.stringify(req.params));
		
		//Check sulla request
        if (!req.params) {
            err.message = "Richiesta non valida";
            err.status = 400;
            console.log(err.message);
            res.send(err);
            return res.end();

        }

        if (req.params.length <= 0) {
            err.message = "La stringa � vuota";
            err.status = 400;
            console.log(err.message);
            res.send(err);
            return res.end();
        }
        
		//lettura della targa dai parametri della request (passata nell'url)
        var licensePlate = req.params.targa;
		
		//operazione per avere tutti i Dossiers relativi alla targa passata in input (nella SELECT faccio visualizzare solo i campi client e licensePlate relativi al dossier)
        db.LicensePlate.find({
            'licensePlate': licensePlate
        }).select({
            "client": 1,
            "licensePlate": 1
        }).exec(function(err, doc) {

            if (err) {
                res.send(err);
                res.end();
                return;
            }
            if (!doc || doc.length == 0) {

                res.json({
                    results: "no data"
                });
                res.end();
                return;
            }
            res.json({
                results: doc
            });
            res.end();
            return;
        });

    },

    c: function UpdateLicensePlate(req, res) {

        var err = new Error();

		//Check sulla request
        if (!req.body) {
            err.message = "Richiesta non valida";
            err.status = 400;
            console.log(err.message);
            res.send(err);
            return res.end();

        }

        if (req.body.length <= 0) {
            err.message = "L'array � vuoto";
            err.status = 400;
            console.log(err.message);
            res.send(err);
            return res.end();
        }

        var name = req.body.name;
        var surname = req.body.surname;

		//lettura del file di configurazione
        var AppConfig = {};
        var cfgFile = fs.readFileSync("./conf.json");
        try {
            AppConfig = JSON.parse(cfgFile);
        } catch (e) {
            console.log('something went wrong in parsing config file, error: ' + e);
        }
		
		//lettura della targa dal fle di configurazione
        var licensePlate = AppConfig.licensePlate;

		//I dossiers relativi al nome e al cognome passati nella request
        db.Dossiers.find({
            'client.name': name,
            'client.surname': surname
        }).exec(function(err, docs) {
            
            if (err) {
                res.send(err);
                res.end();
                return;
            }
            if (!docs || !docs.length) {
                res.json({
                    results: "no data"
                });
                res.end();
                return;
            }
            /*
            for(var i=0; i<docs.length; i++){
	
            docs[i].licensePlate=licensePlate;	
            var id= docs[i]._id;
            }    */
			
			//Aggiornamento della targa su tutti i dossiers relativi al clientepassato in richiesta(name,surname) 
            db.Dossiers.update({
                    'client.name': name,
                    'client.surname': surname
                }, {
                    $set: {
                        licensePlate: licensePlate
                    }
                }, {
                    multi: true
                },
                function(err) {
                    if (err) {
                        res.send(err.message);
                        res.end();
                        return;
                    }
                }
            );

            res.json({


                results: docs
            });
            res.end();
            return;

        });

    },
	
	d: function SaveGarage(req,res){
	
	var err = new Error();
        
		//Check sulla request
        if (!req.body) {
            //err.message = "Richiesta non valida";
            err.status = 400;
            console.log(err.message);
            res.send(err);
            return res.end();
        }
		
		if(!Array.isArray(req.body)){
		//err.message = "non � un array";
		err.status = 400;
		console.log(err.message);
		res.send(err);
		return res.end();		
		}
		
		if(req.body.length <=0){
		//err.message = "array � vuoto";
		err.status = 400;
		console.log(err.message);
		res.send(err);
		return res.end();
		}
		//svuota la collection ogni volta che viene chiamato,(passando come parametro {} si eliminano tutti i documenti della collection) 
		/*
        db.Garage.collection.remove({},function(err){
		
		if(err){
			//err.message = "Rimozione dei documenti non riuscita";
			err.status = 500;
			res.send(err);
			res.end();
		    }				
		});
	        */
		
			
			// inserisce tutti i documenti passati nella request 
			//var garage = new db.Garage(req.body);
            //console.log(JSON.stringify(garage));
		    db.Garage.create(req.body, function (err) {

    			if(err) {
        			//err.message = "Inserimento errato";
        			err.status = 500;
        			console.log("handler d:"+ err.message);
        			res.send(err);
        			return res.end();
    			}

    			res.send(req.body);
    			return res.end();
    		});
		    
	    
	},
	
	e: function getOfficine(req,res){
	
	var err = new Error();
		
	if (!req.params) {
            err.message = "Richiesta non valida";
            err.status = 400;
            console.log(err);
            res.send(err.message);
            return res.end();
        }
	
		
		var codiceOfficina = req.params.codice;
		
		console.log(codiceOfficina);
		
		//mi creao un oggetto query grazie al quale imposto la ricerca della find(),nel senso che se il parametro non viene passato manda un oggetto vuoto, altrimenti passa il codice dell'officina come chiave per la find()
		var query;
		if(!req.params.codice){
			query = {};
			}
			else
			   {
			   query = {"code":codiceOfficina};
			   }
		
		// Find specificando nella select di non considerare l'id
		db.Garage.find(query).select({
            "_id": 0
        }).exec(function(err, doc) {

            if (err) {
                res.send(err);
                res.end();
                return;
            }
            if (!doc || doc.length == 0) {
								
                res.json({
                    results: "no data"
                });
                res.end();
                return;
            }
            res.json({
                results: doc
            });
            res.end();
            return;
        });
	
	},
	
	f: function insertDossiers(req,res){

       if (!req.body) {
            //err.message = "Richiesta non valida";
            err.status = 400;
            console.log(err.message);
            res.send(err);
            return res.end();
        }
        
        if(!Array.isArray(req.body)){
        //err.message = "non � un array";
        err.status = 400;
        console.log(err.message);
        res.send(err);
        return res.end();       
        }
        
        if(req.body.length <=0){
        //err.message = "array � vuoto";
        err.status = 400;
        console.log(err.message);
        res.send(err);
        return res.end();
        }

	 db.Dossiers.collection.insert(req.body, function (err) {

        console.log('ashasjhcakjshcaoooooooooooooooooo');

                if(err) {
                    err.message = "Inserimento errato";
                    err.status = 500;
                    console.log("handler f:"+ err.message);
                    res.send(err);
                    return res.end();
                }

                res.send(req.body);
                return res.end();
            });
    },

g: function SaveDossiers(req,res){
    
    var err = new Error();
        
        //Check sulla request
        if (!req.body) {
            //err.message = "Richiesta non valida";
            err.status = 400;
            console.log(err.message);
            res.send(err);
            return res.end();
        }
        
        if(!Array.isArray(req.body)){
        //err.message = "non � un array";
        err.status = 400;
        console.log(err.message);
        res.send(err);
        return res.end();       
        }
        
        if(req.body.length <=0){
        //err.message = "array � vuoto";
        err.status = 400;
        console.log(err.message);
        res.send(err);
        return res.end();
        }
       
            db.Dossiers.create(req.body, function (err) {
                    
                if(err) {
                    //err.message = "Inserimento errato";
                    err.status = 500;
                    console.log("handler d:"+ err.message);
                    res.send(err);
                    return res.end();
                }

                res.send(req.body);
                return res.end();
            });
            
        
    }

};