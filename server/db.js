/*
* My GitProject web application
* Module: db.js, Model definition
*/

//modules import
var mongoose = require('mongoose');
var assert = require('assert');
var fs = require('fs');

// read config file

var AppConfig = {};
var cfgFile = fs.readFileSync("./conf.json");
try {
    AppConfig = JSON.parse(cfgFile);
} catch (e) {
    console.log('something went wrong in parsing config file, error: ' + e);
}

var connString = AppConfig.mongoDb.host+AppConfig.mongoDb.port+AppConfig.mongoDb.db;
var coll_dossiers = AppConfig.mongoDb.collections.dossiers;
var coll_garage = AppConfig.mongoDb.collections.garage;
var coll_user = AppConfig.mongoDb.collections.users;
var coll_groups = AppConfig.mongoDb.collections.groups;
var coll_surveyors = AppConfig.mongoDb.collections.surveyors;
var coll_system = AppConfig.mongoDb.collections.system;


//establish connection
mongoose.connect(connString);
var conn = mongoose.connection;

//defining schemas
var dossiersSchema = mongoose.Schema({
        '_id': String,
		'dossierId' : String,
		'delegacy' : Object,
		'client' : Object,
		'garage' : Object,
		'surveyor' : Object,
		'examination' : Object,
		'status' : String,
		'notes' : String,
		'invoicePrinted' : Boolean,
		'licensePlate' : String,
		'price' : Number,
		'discount' : Number,
		'amountPaid' : Number,
		'lastUpdate' : Number,
		'history' : Array,
		'appointment' : Number

    }, {collection: coll_dossiers.dossiers});

	
var garageSchema = mongoose.Schema({
	'name': {
		type: String
	},
	'code': {
		type: String
	},
	'address': {
		type: Object
	}	
}, {collection: coll_garage.garage});	


var userSchema = mongoose.Schema({
    'name': {
    	type: String
    },
    'surname':{
    	type: String
    },
    'username': {
    	type: String
    },
    'email': {
    	type: String
    },
    'password':{
    	type: String
    },
    'group':{
    	type: String
    },
    'permissions':{
    	type: Object
    }
}, {collection: coll_user.users});


var groupsSchema = mongoose.Schema({
	'name' : {
		type: String
	},
	'permissions': {
		type: Object
	}
},{collection: coll_groups.groups});


var surveyorsShema = mongoose.Schema({
	'name': {
		type: String
	},
	'surname':{
		type: String
	},
	'registrationNumber':{
		type: String
	},
	'phones':{
		type: Array	
	},
	'faxes':{
		type: Array
	},
	'emails':{
		type: Array
	},
	'pecs':{
		type: Array
	},
	'webs':{
		type: Array
	},
	'garages':{
		type: Array
	}
},{collection: coll_surveyors.surveyors});

var systemSchema = mongoose.Schema({
	'docType': {
		type: String
	},
	'applicationCode':{
		type: String
	},
	'clientIdSerial':{
		type: Number
	},
	'iva':{
		type: Number
	},
	'price':{
		type: Number
	},
	'attributableDelegacy':{
		type: Number
	},
	'attributableSurveyor':{
		type: Number
	},
	'attributableGarage':{
		type: Number
	},
	'attributableAc':{
		type: Number
	},
	'priceConfiguration':{
		type: Array
	},
	'attributableConfiguration':{
		type: Array
	}
},{collection: coll_system.system});	

//Gestione errori
garageSchema.pre('save', function(next) {
console.log('entrato');
var err=new Error();
	var garage=this;
	if(garage.name=="")
	{
	err.message="invalid name";
	return next(err);
	}
	if(garage.code=="")
	{
	err.message="invalid code";
	return next(err);
	}
	if(garage.address.country=="" || garage.address.province=="" || garage.address.locality=="") //e via scorrendo l'oggetto address....
	{
	err.message="invalid address";
	return next(err);
	}
	next();
});

userSchema.pre('save', function(next){
    var err=new Error();
    var user= this;

    if(user.name=="")
    {
    	err.message="invalid name";
    	return next(err);
    }
    if(user.surname=="")
    {
    	err.message="invalid surname";
    	return next(err);	
    }
    if(user.username=="")
    {
    	err.message="invalid username";
		return next(err);    
    }
    if(user.email=="")
    {
    	err.message="invalid email";
		return next(err);    
    }
    if(user.password=="")
    {
    	err.message="invalid password";
    	return next(err);
    }
    if(user.group=="")
    {
    	err.message="invalid group";
    	return next(err);
    }

});

groupsSchema.pre('save', function(next){
	var err=new Error();
	var groups=this;

	if(groups.name=="")
	{
		err.message="invalid group";
    	return next(err);
	}
	if(typeof groups.permissions!==Object)
	{
		err.message="invalid permissions";
		return next(err);
	}
});

surveyorsShema.pre('save', function(next){
	var err =new Error();
	var surveyor=this;

	if(surveyor.name=="")
	{
		err.message="invalid surveyor name";
		return next(err);
	}
	if(surveyor.surname=="")
	{
		err.message="invalid surveyor surname";
		return next(err);
	}
	if(surveyor.registrationNumber=="")
	{
		err.message="invalid surveyor registrationNumber";
		return next(err);
	}
	if(typeof surveyor.phones!== Array)
	{
		err.message="invalid surveyor phones";
		return next(err);
	}
	if(typeof surveyor.faxes!== Array)
	{
		err.message="invalid surveyor faxes";
		return next(err);
	}
	if(typeof surveyor.emails!== Array)
	{
		err.message="invalid surveyor phones";
		return next(err);
	}
	if(typeof surveyor.pecs!== Array)
	{
		err.message="invalid surveyor pecs";
		return next(err);
	}
	if(typeof surveyor.webs!== Array)
	{
		err.message="invalid surveyor phones";
		return next(err);
	}
	if(typeof surveyor.garages!== Array)
	{
		err.message="invalid surveyor garages";
		return next(err);
	}

});

dossiersSchema.pre('save', function(next){
	var err=new Error();
	var dossier=this;
	console.log(dossier.price);

// procedura di aggiornamento del campo price di un dossier con il medesimo della collection price


var myDocument = System.findOne();


console.log(JSON.parse(myDocument));




//fine procedura


	return next();
	
});


//defining Models
var Dossiers = mongoose.model('Dossiers', dossiersSchema);
var Garage = mongoose.model('Garage', garageSchema);
var System = mongoose.model('System', systemSchema);


//exports
exports.Dossiers = Dossiers;
exports.Garage = Garage;
exports.System = System;
